package main

import (
	"fmt"

	"gitlab.reutlingen-university.de/schmolli/joker/joke"
	"gitlab.reutlingen-university.de/schmolli/joker/witz"
)

func main() {
	fmt.Println(joke.GetJoke())
	fmt.Println(witz.GetWitz())
}
