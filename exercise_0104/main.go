package main

import (
	"fmt"
	"os"
)

func main() {
	PrintGreeting()
	PrintGreeting2()
	PrintGreeting3()
}

func PrintGreeting() {
	var nr = 1
	var name = "Marcus"
	fmt.Printf("%03d: Hallo, %s\n", nr, name)
}

func PrintGreeting2() {
	var nr = 1
	var name = "Marcus"
	s := fmt.Sprintf("%03d: Hallo, %s\n", nr, name)
	fmt.Println(s)
}

func PrintGreeting3() {
	file, _ := os.Create("foo.txt")
	var nr = 1
	var name = "Marcus"
	fmt.Fprintf(file, "%03d: Hallo, %s\n", nr, name)
	file.Close()
}
