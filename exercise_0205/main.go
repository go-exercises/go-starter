package main

import (
	"fmt"
	"os"
)

func printSlice(s []string) {
	fmt.Printf("%p –len: %d cap: %d %#v\n", s, len(s), cap(s), s)
}

/*
	func main() {
		if len(os.Args) > 1 {
			printSlice(os.Args)
		} else {
			fmt.Printf("Usage: %s <sentence>\n", os.Args[0])
		}
	}
*/
func main() {
	noOfArgs := len(os.Args)
	switch noOfArgs {
	case 1:
		fmt.Printf("Usage: %s <sentence>\n", os.Args[0])
	default:
		printSlice(os.Args)

	}
}
