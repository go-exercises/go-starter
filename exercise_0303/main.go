package main

import "fmt"

type Point struct {
	x int
	y int
}

type Rectangle struct {
	origin Point
	width  float32
	height float32
}

func main() {

	rectangle := Rectangle{
		origin: Point{
			x: 10,
			y: 10,
		},
		width:  27.9,
		height: 44.876,
	}

	fmt.Printf("%v\n", rectangle)

}
