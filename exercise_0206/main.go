package main

import (
	"fmt"
	"os"
	"unicode/utf8"
)

func printSlice(s []string) {
	fmt.Printf("%p –len: %d cap: %d %#v\n", s, len(s), cap(s), s)
}

func main() {
	if len(os.Args) > 1 {
		printSlice(os.Args)
		numberOfRunes := 0
		numberOfBytes := 0
		for _, word := range os.Args[1:] {
			numberOfBytes += len(word)
			numberOfRunes += utf8.RuneCountInString(word)
		}
		fmt.Printf("Number of bytes: %d\n", numberOfBytes)
		fmt.Printf("Number of runes: %d\n", numberOfRunes)
		fmt.Printf("Average number of bytes per rune: %f\n", float32(numberOfBytes)/float32(numberOfRunes))
	} else {
		fmt.Printf("Usage: %s <sentence>\n", os.Args[0])
	}
}
