package main

import (
	"fmt"
	"os"
)

func printSlice(s []string) {
	fmt.Printf("%p –len: %d cap: %d %#v\n", s, len(s), cap(s), s)
}

func main() {
	printSlice(os.Args)
}
