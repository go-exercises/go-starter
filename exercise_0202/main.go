package main

import "fmt"

const (
	_ = 1 << (10 * iota)
	KIB
	MIB
	GIB
)

func main() {
	fmt.Println(KIB)
	fmt.Println(MIB)
	fmt.Println(GIB)
}
