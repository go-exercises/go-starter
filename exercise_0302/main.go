package main

import "fmt"

type calcFunc func(a, b int) int

func compute(calc calcFunc, init int, a ...int) (result int) {
	result = init
	for _, n := range a {
		result = calc(result, n)
	}
	return
}

func selectCalcFunc(calcOp rune) calcFunc {
	return func(a, b int) int {
		switch calcOp {
		case '+':
			return a + b
		case '*':
			return a * b
		case '-':
			return a - b
		default:
			panic("Illegal operation")
		}
	}
}

func main() {
	fmt.Println(compute(selectCalcFunc('+'), 0, 1, 2, 3))
	fmt.Println(compute(selectCalcFunc('-'), 0, 1, 2, 3))
	fmt.Println(compute(selectCalcFunc('*'), 1, 1, 2, 3))
	fmt.Println(compute(selectCalcFunc('?'), 1, 1, 2, 3))
}
