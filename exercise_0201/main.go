package main

import (
	"fmt"

	"gitlab.reutlingen-university.de/go-exercises/go-starter/exercise_0201/foobar"
)

func main() {
	fmt.Println(foobar.Bar)
	//Variable foobar.foo is not visible: fmt.Println(foobar.far)
}
