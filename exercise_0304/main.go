package main

import "fmt"

type Areaer interface {
	Area() int
}

type Rectangle struct {
	length int
	width  int
}

func (r Rectangle) Area() int {
	return r.length * r.width
}

func (r *Rectangle) SetWidth(width int) {
	r.width = width
}

func (r Rectangle) String() string {
	return fmt.Sprintf("{l: %d, w: %d}", r.length, r.width)
}

func createShape() Areaer {
	return Rectangle{length: 10, width: 5}
}

func main() {
	r := Rectangle{length: 10, width: 5}
	r.SetWidth(9)
	fmt.Println(r.Area())
	s := createShape()
	fmt.Println(s.Area())
	fmt.Println(r)
}
