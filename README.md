# Solutions to the starter course with Go

This repository contains several solutions to exercises of the Go starter course.

The numbers corrspond to the learning modules and units.
E.g. folder exercise_0104 contains the solutions of the exercises of module 01 and unit 04.

Some solutions are missing, since the exercise was just to test the code
from the video or slide.

Some solutions to final units are missing, too, because we'll discuss them in the class room.
