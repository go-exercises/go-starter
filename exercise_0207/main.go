package main

import (
	"fmt"
	"os"
	"unicode/utf8"
)

func printSlice(s []string) {
	fmt.Printf("%p –len: %d cap: %d %#v\n", s, len(s), cap(s), s)
}

func oneOrMore(words []string) bool {
	return len(words) > 1
}

func getNumberOfBytesAndRunes(words []string) (numberOfBytes int, numberOfRunes int) {
	for _, word := range os.Args[1:] {
		numberOfBytes += len(word)
		numberOfRunes += utf8.RuneCountInString(word)
	}
	return
}

func getAverageNumberOfBytesPerRune(numberOfBytes int, numberOfRunes int) float32 {
	return float32(numberOfBytes) / float32(numberOfRunes)
}

func main() {
	if oneOrMore(os.Args) {
		printSlice(os.Args)
		numberOfBytes, numberOfRunes := getNumberOfBytesAndRunes(os.Args)
		fmt.Printf("Number of bytes: %d\n", numberOfBytes)
		fmt.Printf("Number of runes: %d\n", numberOfRunes)
		fmt.Printf("Average number of bytes per rune: %f\n", getAverageNumberOfBytesPerRune(numberOfBytes, numberOfRunes))
	} else {
		fmt.Printf("Usage: %s <sentence>\n", os.Args[0])
	}
}
