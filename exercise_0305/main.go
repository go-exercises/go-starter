package main

import "fmt"

type PhoneBook struct {
	entries map[int]string
}

func (pb *PhoneBook) AddNumbers(person string, numbers ...int) {
	for _, number := range numbers {
		pb.entries[number] = person
	}
}

func CreateEmptyPhoneBook() PhoneBook {
	emptyPB := PhoneBook{
		entries: make(map[int]string),
	}
	return emptyPB
}

func (pb *PhoneBook) PrintPhoneBook() {
	for number, person := range pb.entries {
		fmt.Printf("%s: %d\n", person, number)
	}
}

func main() {

	pb := PhoneBook{
		entries: map[int]string{
			4711: "Marcus",
			4712: "Martin",
		},
	}

	pb.AddNumbers("Dominik", 4713, 4714)
	pb.PrintPhoneBook()

	pb2 := CreateEmptyPhoneBook()
	pb2.AddNumbers("Karlo", 1, 2, 3, 4, 5)
	pb2.PrintPhoneBook()

}
