package main

import (
	"fmt"
)

type BankAccount struct {
	balance float32
	limit   float32
}

func (ba *BankAccount) Withdraw(amount float32) error {

	if amount > ba.balance+ba.limit {
		err := BankAccountWithdrawalError{
			Amount:  amount,
			Balance: ba.balance,
			Limit:   ba.limit,
			Msg:     "amount currently not available",
		}
		return err
	} else {
		ba.balance -= amount
		return nil
	}
}

type BankAccountWithdrawalError struct {
	Amount  float32
	Balance float32
	Limit   float32
	Msg     string
}

func (e BankAccountWithdrawalError) Error() string {
	return fmt.Sprintf("%s: %f > %f", e.Msg, e.Amount, e.Balance+e.Limit)
}

func main() {
	ba := BankAccount{
		balance: 1000.0,
		limit:   3000.0,
	}
	fmt.Printf("%v\n", ba)
	if err := ba.Withdraw(5000); err != nil {
		fmt.Printf("%s\n", err.Error())
	}
	fmt.Printf("%v\n", ba)
	if err := ba.Withdraw(1000); err != nil {
		fmt.Printf("%s\n", err.Error())
	}
	fmt.Printf("%v\n", ba)
}
