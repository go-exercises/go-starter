package main

import (
	"errors"
	"fmt"
)

type BankAccount struct {
	balance float32
	limit   float32
}

func (ba *BankAccount) Withdraw(amount float32) error {

	if amount > ba.balance+ba.limit {
		return errors.New("amount currently not available")
	} else {
		ba.balance -= amount
		return nil
	}
}

func main() {
	ba := BankAccount{
		balance: 1000.0,
		limit:   3000.0,
	}
	fmt.Printf("%v\n", ba)
	if err := ba.Withdraw(5000); err != nil {
		fmt.Println(err.Error())
	}
	fmt.Printf("%v\n", ba)
	if err := ba.Withdraw(1000); err != nil {
		fmt.Println(err.Error())
	}
	fmt.Printf("%v\n", ba)
}
