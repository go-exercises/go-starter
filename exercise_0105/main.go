package main

import (
	"fmt"

	"gitlab.reutlingen-university.de/schmolli/health"
)

func main() {
	fmt.Println(health.Echo("Hi from other package"))
}
